package com.company;

import java.util.Scanner;

public class IO {

    public double solicitarInformacao(String frase) {

        double informacao;

        Scanner scanner = new Scanner(System.in);
        System.out.println(frase);
        informacao = scanner.nextDouble();

        return informacao;
    }

    public void exibirInformacao(String mensagem){
        System.out.println(mensagem);
    }
}