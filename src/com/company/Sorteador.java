package com.company;

import java.util.Random;

public class Sorteador {

    public Simbolos sortear()
    {
        Random sorteio = new Random();
        Simbolos simboloSorteado = Simbolos.values()[sorteio.nextInt(Simbolos.values().length)];
        return simboloSorteado;
    }
}