package com.company;

import java.util.ArrayList;

public class Maquina {

    IO io = new IO();

    public void iniciarMaquina(int quantidadeSlots){

        Sorteador sorteador = new Sorteador();
        ArrayList<String> simbolosorteado = new ArrayList<>();
        Simbolos simbolo;
        int totalPontos = 0;
        int contador = 0;

        for(int i=1; i <= quantidadeSlots; i++)
        {
            simbolo = sorteador.sortear();

            simbolosorteado.add(simbolo.toString());
            io.exibirInformacao("Slot " + i + " sorteou o item " + simbolo);
            totalPontos += simbolo.getValue();
        }

        for(int j=0; j < simbolosorteado.size(); j++){

            if(simbolosorteado.get(0).equals(simbolosorteado.get(j))){
                contador++;
            }
        }

        if(contador == simbolosorteado.size())
            totalPontos = totalPontos * 100;

        io.exibirInformacao("O total de pontos foi: " + totalPontos);
    }
}




