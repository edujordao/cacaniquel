package com.company;

public enum Simbolos {

        BANANA(10),
        FRAMBOESA(50),
        MOEDA(100),
        SETE(300);

        private int value;

        Simbolos(int i) {
            this.value = i;
        }

        public int getValue() {
            return value;
        }
}